package com.Project.UserService;

import org.hibernate.SessionFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@EnableDiscoveryClient
@SpringBootApplication
//@Import(AccountsWebApplication.class)
public class UserServiceApplication {

	public static SessionFactory sessionFactory;

	public static void main(String[] args) {
		// Will configure using accounts-server.yml
		System.setProperty("spring.config.name", "user-server");

		SpringApplication.run(UserServiceApplication.class, args);

	}
}