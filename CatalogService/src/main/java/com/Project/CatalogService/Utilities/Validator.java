package com.Project.CatalogService.Utilities;

import com.Project.CatalogService.Database.WritersRepository;

import java.util.Calendar;

public class Validator {

    public static void validateWriter(String name, Integer born, Integer deceased, String picture, WritersRepository writersRepository) throws IllegalArgumentException {

        if (name == null || name.equals("")) {
            throw new IllegalArgumentException("Invalid name!");
        }

        if (writersRepository.findByName(name).size() != 0) {
            throw new IllegalArgumentException("Writer already present!");
        }

        if (born == null || born < 1900 || born > Calendar.getInstance().get(Calendar.YEAR)) {
            throw new IllegalArgumentException("Invalid born year!");
        }

        if (deceased != null && deceased > Calendar.getInstance().get(Calendar.YEAR)) {
            throw new IllegalArgumentException("Invalid deceased year!");
        }

        if (picture != null && !picture.startsWith("data:image/jpeg;base64")) {
            throw new IllegalArgumentException("Invalid base64 format!");
        }
    }
}
