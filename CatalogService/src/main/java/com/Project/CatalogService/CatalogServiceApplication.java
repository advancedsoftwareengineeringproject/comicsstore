package com.Project.CatalogService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@EnableDiscoveryClient
//@Import(AccountsWebApplication.class)
@SpringBootApplication
public class CatalogServiceApplication {

	public static org.hibernate.SessionFactory sessionFactory;
	// Useless now? //public static java.util.HashMap<javax.servlet.http.HttpSession,com.Project.CatalogService.Database.Clients> loggedUsers;
	public static java.util.HashMap<javax.servlet.http.HttpSession,java.util.HashMap<java.lang.Integer,java.lang.Integer>> cart;

	public static void main(String[] args) {

		System.setProperty("spring.config.name", "catalog-server");

		SpringApplication.run(CatalogServiceApplication.class, args);
	}
}