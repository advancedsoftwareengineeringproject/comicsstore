package com.Project.CatalogService.Database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.Project.CatalogService.Database.Series;
import org.springframework.stereotype.Repository;

import javax.persistence.NamedQuery;
import java.util.List;
import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete
@Repository
public interface SeriesRepository extends JpaRepository<Series, Integer> {

    @Query(value = "select * from Series where title = ?1", nativeQuery = true)
    List<Series> findByTitle(String title);

    @Query("select u from Series u where u.publisher.id = ?1")
    List<Series> findByPublisher(int id);
}
