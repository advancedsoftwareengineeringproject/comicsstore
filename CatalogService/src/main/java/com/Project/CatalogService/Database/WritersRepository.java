package com.Project.CatalogService.Database;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete
@Repository
public interface WritersRepository extends JpaRepository<Writer, Integer> {

    @Query(value = "SELECT * FROM Writers WHERE name = ?1", nativeQuery = true)
    List<Writer> findByName(String name);

}
