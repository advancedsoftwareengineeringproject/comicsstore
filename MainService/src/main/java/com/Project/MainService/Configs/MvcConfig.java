package com.Project.MainService.Configs;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;

@Configuration
@EnableWebMvc
public class MvcConfig extends WebMvcConfigurerAdapter {
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {

        System.out.println(System.getProperty("java.class.path"));

        registry
                .addResourceHandler("/images/**")
                .addResourceLocations("classpath:/images/")
        .setCachePeriod(999999999);
    }
}