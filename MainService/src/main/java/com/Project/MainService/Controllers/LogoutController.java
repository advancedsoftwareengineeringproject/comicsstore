package com.Project.MainService.Controllers;

import com.Project.MainService.Utilities.ServiceType;
import com.Project.MainService.Utilities.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class LogoutController {

    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/logoutAction")
    public void logout(HttpServletRequest request, HttpServletResponse resp) throws IOException {

        restTemplate.getForEntity(Utils.getDataURL(ServiceType.USER_SERVICE, "clients/logout?sessionId=" + request.getSession().getId()), String.class);
        resp.sendRedirect("/");

    }

}
