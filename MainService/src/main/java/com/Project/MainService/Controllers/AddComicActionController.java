package com.Project.MainService.Controllers;

import com.Project.MainService.Utilities.ServiceType;
import com.Project.MainService.Utilities.Utils;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.StringUtils;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

@RestController
public class AddComicActionController {

    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/addcomicAction")
    public void addcomicAction(@RequestParam(name = "cover", required = false) MultipartFile file, @RequestParam(name = "preview", required = false) String previewLink, @RequestParam(name = "writer", required = false) String writerName, @RequestParam(name = "price", required = false) String price, @RequestParam(name = "issue", required = false) String issueNumber, @RequestParam(name = "sel1", required = false) String serie, @RequestParam(name = "title", required = false) String title, HttpServletRequest request, HttpServletResponse response) throws IOException {

        StringBuilder sb = new StringBuilder();
        sb.append("data:image/png;base64,");
        sb.append(StringUtils.newStringUtf8(Base64.encodeBase64(file.getBytes(), false)));

        Map<String, String> mapped = new HashMap<>();
        mapped.put("cover", sb.toString());
        mapped.put("writer", writerName);
        mapped.put("price", price);
        mapped.put("issueNumber", issueNumber);
        mapped.put("sel1", serie);
        mapped.put("title", title);

        restTemplate.postForEntity(Utils.getDataURL(ServiceType.CATALOG_SERVICE, "/issues/add/issue"), mapped, Void.class);
        response.sendRedirect("/");

    }

}
