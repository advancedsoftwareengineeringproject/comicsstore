package com.Project.MainService.Controllers;

import com.Project.MainService.Utilities.ServiceType;
import com.Project.MainService.Utilities.Utils;
import org.apache.http.HttpResponse;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class AddToCartController {

    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/addToCart")
    public void cart(@RequestParam(name = "issueId") String issueId, @RequestParam(name = "isCart") String isCart, HttpServletRequest request, HttpServletResponse resp) throws IOException {

        restTemplate.getForEntity(Utils.getDataURL(ServiceType.ORDER_SERVICE, "cart/add?sessionId=" + request.getSession().getId() + "&issueId=" + issueId), String.class);
        if(isCart.equals("false"))
            resp.sendRedirect("/comics?id=" + issueId);
        else
            resp.sendRedirect("/cart");

    }

}
