package com.Project.MainService.Controllers;

import com.Project.MainService.Utilities.ServiceType;
import com.Project.MainService.Utilities.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class OrderCartController {

    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/placeOrder")
    public void order(@RequestParam(name = "address", required = false) String address, @RequestParam(name = "card", required = false) String card, @RequestParam(name = "cvc", required = false) String cvc, @RequestParam(name = "message", required = false) String message, HttpServletRequest request, HttpServletResponse resp) throws IOException {

        ResponseEntity<String> response = restTemplate.getForEntity(Utils.getDataURL(ServiceType.ORDER_SERVICE, "cart/buy?sessionId=" + request.getSession().getId() + "&address=" + address + "&message=" + message), String.class);
        String totalPrice = new Gson().fromJson(response.getBody(), JsonObject.class).get("body").getAsString();
        System.out.println(totalPrice);
        resp.sendRedirect("?emptyCart=true&price=" + Double.valueOf(totalPrice));
    }

}
