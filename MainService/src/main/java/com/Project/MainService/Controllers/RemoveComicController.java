package com.Project.MainService.Controllers;

import com.Project.MainService.Utilities.ServiceType;
import com.Project.MainService.Utilities.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@RestController
public class RemoveComicController {

    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/removecomic")
    @Timed(value = "removeComic.time", description = "Time taken to remove a comic")
    public void cart(@RequestParam(name = "issueId") String issueId, HttpServletRequest request, HttpServletResponse resp) throws IOException {

        restTemplate.getForEntity(Utils.getDataURL(ServiceType.CATALOG_SERVICE, "issues/delete/" + issueId), String.class);
        resp.sendRedirect("/");

    }

}
