package com.Project.MainService.Controllers;

import com.Project.MainService.Utilities.ServiceType;
import com.Project.MainService.Utilities.Utils;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import io.micrometer.core.annotation.Timed;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class LoginController {

    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/login")
    //@Timed(value = "loginPageLoad.time", description = "Time taken to show the login page.")
    public ModelAndView login(@RequestParam(name = "success", required = false) boolean success, @RequestParam(name = "email", required = false) String email, @RequestParam(name = "pswd", required = false) String pswd, HttpServletRequest request, HttpServletResponse resp) throws IOException {

        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("login.html");

        Utils.addClientToModel(modelAndView, request, restTemplate);

        modelAndView.addObject("success", success);

        if(email != null && pswd != null) {

            ResponseEntity<String> response = restTemplate.getForEntity(Utils.getDataURL(ServiceType.USER_SERVICE, "clients/login?" +
                    "email=" + email + "&" +
                    "pswd=" + pswd + "&" +
                    "sessionId=" + request.getSession().getId()), String.class);

            JsonObject jsonObject = new Gson().fromJson(response.getBody(), JsonObject.class).get("body").getAsJsonObject();

            boolean exists = !jsonObject.get("id").getAsString().equals("-1");

            modelAndView.addObject("failed", !exists);
            modelAndView.addObject("logged", exists);

            if(exists) {
                resp.sendRedirect("/");
            }

        }

        return modelAndView;

    }

}
