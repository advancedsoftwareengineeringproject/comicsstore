package com.Project.MainService.Controllers;

import com.Project.MainService.Utilities.ServiceType;
import com.Project.MainService.Utilities.Utils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@RestController
public class RemoveFromCartController {

    RestTemplate restTemplate = new RestTemplate();

    @RequestMapping("/removeFromCart")
    public void cart(@RequestParam(name = "issueId") String issueId, @RequestParam(name = "isCart") String isCart, HttpServletRequest request, HttpServletResponse resp) throws IOException {

        restTemplate.getForEntity(Utils.getDataURL(ServiceType.ORDER_SERVICE, "cart/remove?sessionId=" + request.getSession().getId() + "&issueId=" + issueId), String.class);
        if(isCart.equals("false"))
            resp.sendRedirect("/comics?id=" + issueId);
        else
            resp.sendRedirect("/cart");

    }

    @RequestMapping("/removeAllFromCart")
    public void cartAll(@RequestParam(name = "isCart") String isCart, HttpServletRequest request, HttpServletResponse resp) throws IOException {

        restTemplate.getForEntity(Utils.getDataURL(ServiceType.ORDER_SERVICE, "cart/removeAll?sessionId=" + request.getSession().getId()), String.class);
        if(isCart.equals("false"))
            resp.sendRedirect("/");
        else
            resp.sendRedirect("/cart");

    }

}
